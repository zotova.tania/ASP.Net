﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practice2
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (RadioButtonMale.Checked)
            {
                LabelChecked.Text = "Male was selected";
            }
            else if (RadioButtonFemale.Checked)
            {
                LabelChecked.Text = "Female was selected";
            }
            else
            {
                LabelChecked.Text = "Was not selected";
            }

        }
    }
}