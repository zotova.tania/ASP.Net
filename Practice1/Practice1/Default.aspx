﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Practice1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Practice 1</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="GlobalEvents" runat="server" GroupingText="Global Events">
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" style="margin-bottom: 0px" Text="Terminate session" />
            <br />
        </asp:Panel>
        <asp:Panel ID="StateManagement" runat="server" GroupingText="State Management">
            <br />
            <asp:Label ID="Label2" runat="server" Text="Click counters"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text ="ViewState is default"></asp:Label>
            <asp:Button ID="Button2" runat="server" Text="0" OnClick="Button2_Click" />
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text ="ViewState is false  "></asp:Label>
            <asp:Button ID="Button3" runat="server" ViewState="false" Text="0" OnClick="Button3_Click" />
            <br />
        </asp:Panel>
    </div>
        <asp:Panel ID="QuadraticEquation" runat="server" GroupingText="Quadratic Equation">
            <br />
            <asp:Label ID="Label5" runat="server" Text="a*x2+bx+c=0"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label6" runat="server" Text ="a"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text ="b"></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label8" runat="server" Text ="c"></asp:Label>
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button4" runat="server" Text="Calculate X" OnClick="Button4_Click" />
            <br />
            <br />
            <asp:Label ID="Label9" runat="server" Text =""></asp:Label>
            <br />
        </asp:Panel>
        <asp:Panel ID="PanelRequest" runat="server" Height="137px" GroupingText="Request/Responce">
            <asp:Label ID="LabelRequest" runat="server" Text=""></asp:Label><br />
            <asp:Label ID="LabelResponce" runat="server" Text=""></asp:Label><br />
        </asp:Panel>
    </form>
</body>
</html>
