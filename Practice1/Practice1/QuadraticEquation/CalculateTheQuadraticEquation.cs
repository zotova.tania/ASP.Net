﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practice1.QuadraticEquation
{
    public static class QuadraticEquation
    {
        //private static float a, b, c, d;
        //private static int caseD;
        //private static string result;
        public static string Calculate(string aText, string bText, string cText)
        {
            float a, b, c;
            string result;
            if (float.TryParse(aText, out a))
            {
                if (a == 0) return "a can't be 0";

                if (float.TryParse(bText, out b))
                {
                    if (float.TryParse(cText, out c))
                    {
                        result = SolveEquation(a, b, c);
                    }
                    else
                    {
                        return "c must be a float";
                    }
                }
                else
                {
                    return "b must be a float";
                }
            }
            else
            {
                return "a must be a float";
            }

            return result;
        }
        private static string SolveEquation(float a, float b, float c)
        {
            float d = b * b - 4 * a * c;
            string result;

            if (d > 0)
            {
                result = "The equation has 2 distinct real roots: ";
                result += " x1 = ";
                result += String.Format("{0:0.####}", ((-b + Math.Sqrt(d)) / 2 / a));
                result += " and ";
                result += "x2 = ";
                result += String.Format("{0:0.####}", ((-b - Math.Sqrt(d)) / 2 / a));
            }
            else if (d == 0)
            {
                result = "The equation has 2 coinciding real roots: ";
                result += "x = ";
                result += String.Format("{0:0.####}", ((-b) / 2 / a));
            }
            else {
                result = "The equation has 2 distinct imaginary roots.";
            }
            return result;
        }
    }
}