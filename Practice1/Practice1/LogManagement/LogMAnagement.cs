﻿using System;
using System.IO;

namespace Practice1
{
    public static class LogManagement
    {
        public static string filePath = AppDomain.CurrentDomain.BaseDirectory + @"SessionsLogs\allsessionskeeper.txt";
        public static void addToFile(string st)
        {
            File.AppendAllLines(filePath,new string[] {st});
        }
    }
}