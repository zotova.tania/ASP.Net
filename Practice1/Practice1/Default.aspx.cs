﻿using System;
using System.Activities.Statements;

namespace Practice1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = " Sessions log file is stored at : "+LogManagement.filePath;
            Button3.Text = Session["field1"].ToString();
            LabelRequest.Text = Request.InputStream.ToString();
            LabelResponce.Text = Response.OutputStream.ToString();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Button2.Text = (Int32.Parse(Button2.Text) + 1).ToString();
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            uint current;
            if (UInt32.TryParse(Session["field1"].ToString(), out current))
                {
                current++;
                Session["field1"] = current;
                Button3.Text=Session["field1"].ToString();
            }
           else throw new Exception("Can't add more clicks");

        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            Label9.Text = Practice1.QuadraticEquation.QuadraticEquation.Calculate(TextBox1.Text, TextBox2.Text, TextBox3.Text);
        }

    }
}