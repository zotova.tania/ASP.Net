﻿using System;

namespace Practice1
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            LogManagement.addToFile(DateTime.Now + " Start session with ID: " + Session.SessionID);
            Session["field1"] = "0";
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            LogManagement.addToFile(DateTime.Now + " End   session with ID: " + Session.SessionID);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Session.Abandon();
        }
    }
}